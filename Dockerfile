FROM dmonakhov/alpine-fio

VOLUME /tmp
WORKDIR /tmp
COPY ./entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
CMD ["fio"]
