# Disk Bench

Benchmark Kubernetes persistent disk volumes with `fio`: Read/write IOPS, bandwidth MB/s and latency. Based on [this](https://github.com/leeliu/dbench) repo.

## Usage

1. Download [dbench.yaml](./dbench.yaml) and change the default parameters if you want.
2. Deploy using: `kubectl apply -f dbench.yaml`
3. Once deployed, the Dbench Job will:
    * provision a Persistent Volume of `1000Gi`
    * run a series of `fio` tests on the newly provisioned disk
    * currently there are 9 tests, 15s per test - total runtime is ~2.5 minutes
4. Follow benchmarking progress using: `kubectl logs -f job/dbench`
5. At the end of all tests, you'll see a summary

```txt
==================
= Dbench Summary =
==================
Random Read/Write IOPS: 4967/928. BW: 220MiB/s / 99.7MiB/s
Average Latency (usec) Read/Write: 932.90/4279.82
Sequential Read/Write: 221MiB/s / 105MiB/s
Mixed Random Read/Write IOPS: 2789/925
```

Once the tests are finished, clean up using: `kubectl delete -f dbench.yaml` and that should deprovision the persistent disk and delete it to minimize storage billing.

## Notes / Troubleshooting

* It's useful to test multiple disk sizes as most cloud providers price IOPS per GB provisioned. So a `4000Gi` volume will perform better than a `1000Gi` volume. Just edit the yaml, `kubectl delete -f dbench.yaml` and run `kubectl apply -f dbench.yaml` again after deprovision/delete completes.
* A list of all `fio` tests are in [docker-entrypoint.sh](./docker-entrypoint.sh).

## Contributors

* Lee Liu (LogDNA)
* [Alexis Turpin](https://github.com/alexis-turpin)
* [Pieter Botha](https://github.com/zacanbot)
